
#include "string.h"

char		*str_mapi(char const *s, char (*f)(size_t, char)) {
	size_t	i;
	size_t	size;
	char	*new;

	size = str_len(s);
	new = str_new(size);
	if (new == NULL)
		return NULL;
	i = 0;
	while (i < size) {
		new[i] = f(i, s[i]);
		++i;
	}
	return new;
}
