
#include "string.h"

void		str_iter(char *s, void (*f)(char *)) {
	size_t	cur;

	cur = 0;
	while (s[cur] != 0) {
		(*f)(&(s[cur]));
		cur++;
	}
}
