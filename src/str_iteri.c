
#include "string.h"

void		str_iteri(char *s, void (*f)(size_t, char *)) {
	size_t	cur;

	cur = 0;
	while (s[cur] != 0) {
		f(cur, s + cur);
		cur++;
	}
}
