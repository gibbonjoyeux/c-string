
#include "string.h"

char		*str_join(char const *s1, char const *s2) {
	size_t	len_1;
	size_t	len_2;
	size_t	total_len;
	char	*new;

	len_1 = (s1) ? str_len(s1) : 0;
	len_2 = (s2) ? str_len(s2) : 0;
	total_len = len_1 + len_2;
	new = str_new(total_len);
	if (new == NULL)
		return NULL;
	if (len_1)
		str_cpy(new, s1);
	if (len_2)
		str_cpy((new + len_1), s2);
	return new;
}
