
#include "string.h"

void	str_del(char **as) {
	if (as == NULL)
		return;
	if (*as != NULL)
		free(*as);
	*as = NULL;
}
