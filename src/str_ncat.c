
#include "string.h"

char		*str_ncat(char *s1, const char *s2, size_t n) {
	size_t	cur;
	size_t	cur_2;

	cur = 0;
	cur_2 = 0;
	while (s1[cur] != 0)
		cur++;
	while (cur_2 < n && s2[cur_2] != 0) {
		s1[cur] = s2[cur_2];
		cur_2++;
		cur++;
	}
	s1[cur] = 0;
	return s1;
}
