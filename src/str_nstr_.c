
#include "string.h"

char		*str_nstr(const char *big, const char *little, size_t len) {
	size_t	cur;
	size_t	cur_2;

	if (little[0] == 0)
		return ((char *)&(big[0]));
	cur = 0;
	while (big[cur] != 0 && cur < len) {
		while (big[cur] != 0 && big[cur] != little[0] && cur < len)
			cur++;
		cur_2 = 0;
		while (big[cur + cur_2] != 0 && little[cur_2] != 0
		&& big[cur + cur_2] == little[cur_2] && cur < len && cur + cur_2 < len)
			cur_2++;
		if (little[cur_2] == 0 && cur < len) {
			return ((char *)&(big[cur]));
		} else {
			if (big[cur] != 0 && cur < len)
				cur++;
			else
				return NULL;
		}
	}
	return NULL;
}
