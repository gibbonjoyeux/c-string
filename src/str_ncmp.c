
#include "string.h"

int			str_ncmp(const char *s1, const char *s2, size_t n) {
	size_t	cur;

	if (n == 0)
		return 0;
	cur = 0;
	while (s1[cur] != 0 && s2[cur] != 0 && s1[cur] == s2[cur] && cur < n - 1)
		cur++;
	return ((unsigned char)s1[cur] - (unsigned char)s2[cur]);
}
