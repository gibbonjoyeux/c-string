
int		to_upper(int c) {
	if (c >= 'a' && c <= 'z')
		return (c + ('A' - 'a'));
	return c;
}
