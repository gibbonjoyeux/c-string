
#include "string.h"

char		*str_cpy(char *dst, const char *src) {
	size_t	i;

	if (src == NULL)
		return dst;
	i= 0;
	while (src[i] != 0) {
		dst[i] = src[i];
		i++;
	}
	dst[i] = 0;
	return dst;
}
