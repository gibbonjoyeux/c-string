
#include "string.h"

char		*str_rchr(const char *s, int c) {
	int		cur;

	cur = str_len(s);
	while (s[cur] != c && cur >= 0)
		cur--;
	if (cur >= 0 || c == 0)
		return (char *)&(s[cur]);
	return NULL;
}
