
#include "string.h"

int		is_bracket(int c) {
	if (c == '(' || c == '{' || c == '['
	|| c == ')' || c == '}' || c == ']')
		return c;
	return 0;
}
