
#include "string.h"

char		*str_dup(const char *s) {
	char	*new;
	size_t	size;
	size_t	i;

	if (s == NULL)
		return NULL;
	size = str_len(s);
	new = (char *)str_new(size);
	if (new == NULL)
		return NULL;
	i = 0;
	while (i < size) {
		new[i] = s[i];
		++i;
	}
	return new;
}
