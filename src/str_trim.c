
#include "string.h"

char		*str_trim(char const *s) {
	char	*new;
	size_t	start;
	size_t	end;
	size_t	len;

	len = str_len(s);
	/// COMPUTE STARTING POINT
	start = 0;
	while (s[start] != 0
	&& (s[start] == ' '
	|| s[start] == '\n'
	|| s[start] == '\t'))
		++start;
	if (start == len)
		return str_new(0);
	/// COMPUTE ENDING POINT
	end = len;
	while (end > 0
	&& (s[end - 1] == ' '
	|| s[end - 1] == '\n'
	|| s[end - 1] == '\t'))
		--end;
	/// CREATE SUBSTRING
	new = str_sub(s, start, end - start);
	return new;
}
