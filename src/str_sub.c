
#include "string.h"

char		*str_sub(char const *s, size_t start, size_t len) {
	char	*new;
	size_t	i;

	if (s == NULL)
		return NULL;
	new = str_new(len);
	if (new == NULL)
		return NULL;
	i = 0;
	while (s[start + i] != 0 && i < len) {
		new[i] = s[start + i];
		++i;
	}
	return new;
}
