
#include "string.h"

int			str_equ(char const *s1, char const *s2) {
	size_t	cur;

	if (s1 == NULL || s2 == NULL)
		return (s1 == s2);
	cur = 0;
	while (s1[cur] != 0 && s2[cur] != 0 && s1[cur] == s2[cur])
		cur++;
	if (s1[cur] == 0 && s2[cur] == 0)
		return 1;
	return 0;
}
