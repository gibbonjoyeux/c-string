
#include "string.h"

char	*str_str(const char *big, const char *little) {
	int	cur;
	int	cur_2;

	if (little[0] == 0)
		return ((char *)&(big[0]));
	cur = 0;
	while (big[cur] > 0) {
		while (big[cur] > 0 && big[cur] != little[0])
			cur++;
		cur_2 = 0;
		while (big[cur + cur_2] > 0 && little[cur_2] > 0
		&& big[cur + cur_2] == little[cur_2])
			cur_2++;
		if (little[cur_2] == 0) {
			return ((char *)&(big[cur]));
		} else {
			if (big[cur])
				cur++;
			else
				return NULL;
		}
	}
	return NULL;
}
