
#include "string.h"

char		*str_ncpy(char *dst, const char *src, size_t len) {
	size_t	cur;

	cur = 0;
	while (src[cur] != 0 && cur < len) {
		dst[cur] = src[cur];
		cur++;
	}
	if (cur < len) {
		while (cur < len) {
			dst[cur] = 0;
			cur++;
		}
	}
	return dst;
}
