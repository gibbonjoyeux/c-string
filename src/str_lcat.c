
#include "string.h"

size_t		str_lcat(char *dst, const char *src, size_t size) {
	size_t	cur;
	size_t	cur_2;
	int		size_src;
	int		size_dst;

	size_src = str_len(src);
	size_dst = str_len(dst);
	cur = 0;
	cur_2 = 0;
	while (dst[cur] != 0)
		cur++;
	if (size < cur)
		return size_src + size;
	while (cur < size - 1 && src[cur_2] != 0) {
		dst[cur] = src[cur_2];
		cur_2++;
		cur++;
	}
	dst[cur] = 0;
	return (size_src + size_dst);
}
