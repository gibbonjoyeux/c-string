
#include "string.h"

char		*str_ndup(const char *s1, size_t n) {
	char	*str;
	size_t	i;

	if (s1 == NULL)
		return NULL;
	str = (char *)str_new(n);
	if (str == NULL)
		return NULL;
	i = 0;
	while (i < n) {
		str[i] = s1[i];
		i++;
	}
	return str;
}
