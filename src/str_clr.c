
#include "string.h"

void		str_clr(char *s) {
	size_t	cur;

	cur = 0;
	while (s[cur]) {
		s[cur] = 0;
		cur++;
	}
}
