
#include "string.h"

char		*str_new(size_t size) {
	char	*new;
	size_t	i;

	new = (char *)malloc(sizeof(char) * (size + 1));
	if (new == NULL)
		return NULL;
	i = 0;
	while (i <= size) {
		new[i] = 0;
		++i;
	}
	return new;
}
