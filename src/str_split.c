
#include "string.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static char		**get_container(char *s, char c) {
	size_t		count;
	size_t		i;

	i = 0;
	count = 0;
	while (s[i] != 0) {
		/// PASS WHITE
		while (s[i] != 0 && s[i] == c)
			++i;
		/// PASS WORD
		if (s[i] > 0) {
			++count;
			while (s[i] != 0 && s[i] != c)
				++i;
		}
	}
	/// CREATE CONTAINER
	return (char **)calloc(count + 1, sizeof(char *));
}

static void		free_container(char **list, size_t from) {
	while (from > 0) {
		free(list[from]);
		--from;
	}
	free(list);
}

static char		*get_word(char *s, size_t *i, char c) {
	char		*word;
	size_t		len;
	size_t		j;

	/// WORD LENGTH
	len = 0;
	while (s[*i + len] != 0 && s[*i + len] != c)
		++len;
	/// ALLOW WORD
	word = str_new(len);
	if (word == NULL)
		return NULL;
	/// COPY WORD
	j = 0;
	while (s[*i] != 0 && s[*i] != c) {
		word[j] = s[*i];
		(*i)++;
		++j;
	}
	return word;
}

static char		**fill_container(char **list, char *s, char c) {
	size_t		i, j;

	i = 0;
	j = 0;
	while (s[i] != 0) {
		/// PASS WHITE
		while (s[i] != 0 && s[i] == c)
			++i;
		if (s[i] > 0) {
			/// SAVE WORD
			list[j] = get_word(s, &i, c);
			if (list[j] == NULL) {
				free_container(list, j - 1);
				return NULL;
			}
			++j;
		}
	}
	return list;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

char			**str_split(const char *s, char c) {
	char		**list;

	/// CREATE LIST
	if (s == NULL)
		return NULL;
	list = get_container((char *)s, c);
	if (list == NULL)
		return NULL;
	/// FILL LIST
	return fill_container(list, (char *)s, c);
}
