
#include "string.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static char		**get_container(char *s) {
	size_t		count;
	size_t		i;

	i = 0;
	count = 0;
	while (s[i] != 0) {
		/// PASS WHITE
		while (s[i] != 0 && is_space(s[i]) == 1)
			++i;
		/// PASS WORD
		if (s[i] > 0) {
			++count;
			while (s[i] != 0 && is_space(s[i]) == 0)
				++i;
		}
	}
	/// CREATE CONTAINER
	return (char **)calloc(count + 1, sizeof(char *));
}

static void		free_container(char **list, size_t from) {
	while (from > 0) {
		free(list[from]);
		--from;
	}
	free(list);
}

static char		*get_word(char *s, size_t *i) {
	char		*word;
	size_t		len;
	size_t		j;

	/// WORD LENGTH
	len = 0;
	while (s[*i + len] != 0 && is_space(s[*i + len]) == 0)
		++len;
	/// ALLOC WORD
	word = str_new(len);
	if (word == NULL)
		return NULL;
	/// COPY WORD
	j = 0;
	while (s[*i] != 0 && is_space(s[*i]) == 0) {
		word[j] = s[*i];
		(*i)++;
		++j;
	}
	return word;
}

static char		**fill_container(char **list, char *s) {
	size_t		i, j;

	i = 0;
	j = 0;
	while (s[i] != 0) {
		/// PASS WHITE
		while (s[i] != 0 && is_space(s[i]) == 1)
			++i;
		if (s[i] > 0) {
			/// SAVE WORD
			list[j] = get_word(s, &i);
			if (list[j] == NULL) {
				free_container(list, j - 1);
				return NULL;
			}
			++j;
		}
	}
	return list;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

char			**str_splitwhite(const char *s) {
	char		**list;

	/// CREATE LIST
	if (s == NULL)
		return NULL;
	list = get_container((char *)s);
	if (list == NULL)
		return NULL;
	/// FILL LIST
	return fill_container(list, (char *)s);
}
