
#include "string.h"

char	*str_chr(const char *s, int c) {
	int		cur;

	if (s == NULL)
		return NULL;
	cur = 0;
	while (s[cur] != c && s[cur] != 0)
		cur++;
	if (s[cur] != 0 || c == 0)
		return (char *)&(s[cur]);
	return NULL;
}
