
#include "string.h"

int		is_openbracket(int c) {
	if (c == '(' || c == '{' || c == '[')
		return c;
	return 0;
}
