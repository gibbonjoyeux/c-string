
#include "string.h"

int			str_cmp(const char *s1, const char *s2) {
	size_t	cur;

	if (s1 == NULL || s2 == NULL)
		return 0;
	cur = 0;
	while (s1[cur] != 0 && s2[cur] != 0 && s1[cur] == s2[cur])
		cur++;
	return ((unsigned char)s1[cur] - (unsigned char)s2[cur]);
}
