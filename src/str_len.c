
#include "string.h"

size_t		str_len(const char *s) {
	size_t	i;

	if (s == NULL)
		return 0;
	i = 0;
	while (s[i] != 0)
		++i;
	return i;
}
