
#include "string.h"

char		*str_cat(char *s1, const char *s2) {
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	while (s1[i] != 0)
		++i;
	while (s2[j] != 0) {
		s1[i] = s2[j];
		++i;
		++j;
	}
	s1[i] = 0;
	return s1;
}
