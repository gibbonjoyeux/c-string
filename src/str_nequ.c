
#include "string.h"

int			str_nequ(char const *s1, char const *s2, size_t n) {
	size_t	cur;

	cur = 0;
	while (s1[cur] != 0 && s2[cur] != 0 && s1[cur] == s2[cur] && cur < n)
		cur++;
	if ((s1[cur] == 0 && s2[cur] == 0) || cur == n)
		return 1;
	return 0;
}
