#ifndef STRING_H
# define STRING_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stddef.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define IS_PRINT(c)			(c >= 32 && c <= 126)
#define IS_DIGIT(c)			(c >= '0' && c <= '9')
#define IS_ALPHA(c)			((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
#define IS_ALNUM(c)			(IS_DIGIT(c) || IS_ALPHA(c))
#define IS_ASCII(c)			(c >= 0 && c <= 127)
#define IS_SPACE(c)			(c == ' ' || c == '\t' || c == '\n' || c == '\r')
#define IS_QUOTE(c)			(c == '\'' || c == '"' || c == '`')
#define IS_OPENBRACKET(c)	(c == '(' || c == '{' || c == '[')
#define IS_CLOSERACKET(c)	(c == ')' || c == '}' || c == ']')
#define IS_BRACKET(c)		(IS_OPENBRACKET(c) || IS_CLOSEBRACKET(c))

#define TO_UPPER(c)			((c >= 'a' && c <= 'z') ? c + ('A' - 'a') : c)
#define TO_LOWER(c)			((c >= 'A' && c <= 'Z') ? c - ('A' - 'a') : c)

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

char				*str_new(size_t size);
char				*str_dup(const char *src);
char				*str_ndup(const char *s1, size_t n);
char				*str_sub(char const *src, size_t start, size_t len);
char				*str_trim(char const *s);
char				*str_join(char const*s1, char const *s2);
char				*str_map(char const *s, char (*f)(char));
char				*str_mapi(char const *s, char (*f)(size_t, char));
char				**str_split(char const *s, char c);
char				**str_splitwhite(char const *s);
void				str_del(char **as);

void				str_clr(char *s);
size_t				str_len(const char *s);
char				*str_cpy(char *dst, const char *src);
char				*str_ncpy(char *dst, const char *src, size_t len);
char				*str_cat(char *s1, const char *s2);
char				*str_ncat(char *s1, const char *s2, size_t n);
size_t				str_lcat(char *dst, const char *src, size_t size);
char				*str_chr(const char *s, int32_t c);
char				*str_rchr(const char *s, int32_t c);
char				*str_str(const char *big, const char *little);
char				*str_nstr(const char *big, const char *lit, size_t len);
int					str_cmp(const char *s1, const char *s2);
int					str_ncmp(const char *s1, const char *s2, size_t n);
int					str_equ(char const *s1, char const *s2);
int					str_nequ(char const *s1, char const *s2, size_t n);
void				str_iter(char *s, void (*f)(char *));
void				str_iteri(char *s, void (*f)(size_t, char *));

int					is_alpha(int c);
int					is_digit(int c);
int					is_alnum(int c);
int					is_ascii(int c);
int					is_print(int c);
int					is_space(int c);
int					is_quote(int c);
int					is_bracket(int c);
int					is_openbracket(int c);
int					is_closebracket(int c);
int					to_upper(int c);
int					to_lower(int c);

size_t				skip_alpha(char *s);
size_t				skip_digit(char *s);
size_t				skip_alnum(char *s);
size_t				skip_space(char *s);
size_t				skip_print(char *s);

#endif
